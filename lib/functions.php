<?php

/* 
vous ajouterez ici les fonctions qui vous sont utiles dans le site,
je vous ai créé la première qui est pour le moment incomplète et qui devra contenir
la logique pour choisir la page à charger
*/

function getContent($num_page){
        switch ($num_page) {
            case "exo1": require "../pages/exo1.php"; break;
            case "exo2": require "../pages/exo2.php"; break;
            case "exo3": require "../pages/exo3.php"; break;
            case "exo4": require "../pages/exo4.php"; break;
            case "exo5": require "../pages/exo5.php"; break;
            case "exo6": require "../pages/exo6.php"; break;
            case "exo7": require "../pages/exo7.php"; break;
            default: require "../pages/home.php";
        }
    }
    

function getPart($name){
	include __DIR__ . '/../parts/'. $name . '.php';
}





